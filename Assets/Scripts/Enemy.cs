﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Enemy : MonoBehaviour {
	
		public static int NumEnemies { get; private set; }

		public int Health;

		public int Damage;

		void Awake() {
			NumEnemies++;

			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_appear");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		}

		void Update() {
			if (Health <= 0) {
				Destroy(gameObject);
			}
		}

		void OnCollisionEnter2D(Collision2D coll) {
			if (coll.gameObject.CompareTag("PLAYER")) {
				Player player = coll.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
				player.SendMessage("ApplyDamage", Damage); 
			}
		}

		void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_1");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

		void OnDestroy() {
			NumEnemies--;
		}
	}

}

